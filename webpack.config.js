var path = require('path');
var webpack = require('webpack');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  //entry is not needed since it is defined in GULP
  entry: {
    main:'./app/app.bootstrap.js',
    vendor: [ './app/vendorRequirements.js',
              './node_modules/angular/angular.js',
              './node_modules/angular-animate/angular-animate.js',
              './node_modules/@uirouter/angularjs/release/angular-ui-router.js',
              './node_modules/leaflet/dist/leaflet.js',
              './node_modules/d3/build/d3.js']/*
    customcss:['./assets/css/baseStyles.css','./assets/css/layout.css']*/
  },
  output: {
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      // to use js lint on app load. Comes in handy to track errors and bugs
      {
        test: /\.js$/, //type of files to apply linting
        exclude: /node_modules/, //where to exclude linting
        enforce:'pre', //tell it is a pre-loader
        use:[{
          loader: 'jshint-loader' //the loader
        }]
      },
      //to allow import of HTML files( ex: require("./file.html"))
      //https://github.com/webpack-contrib/html-loader
      {
        test: /\.html$/,
        use:[{
          loader:'html-loader',
          options:{
            attrs: false, /*[':custom-src']*/
            minimize:false
          }
        }]
      }
    ],
    loaders: [
      {
        //translate ES2015 JS or JSX to old plain JS
        test:/\.(js|jsx)$/, //check for any file that ends with .js and apply this loader
        loader: 'babel-loader', //the loader
        exclude: /node_modules/, //exclude any files found in this folder. Ignore this folder
        query:{
          presets: ['es2015'] //use babel-loader with ES2015 preset
        }
      },
      //To allow direct import of CSS files. (ex: require('./styles/main.css');)
      {
        test: /\.css$/, //check for any file that ends with .css or scss and apply this loader
        //exclude: /node_modules/, //exclude any files found in this folder. Ignore this folder
        loader:'style!css'
        /*use: [ //the loaders (style-loader, css-loader,sass-loader)
          {loader:'style-loader'},
          {loader:'css-loader'},
          {loader: 'sass-loader'}
        ]*/
      },
      //To allow direct import of image url and use it as a variable (ex: imgElement.src = require('./images/my-image.jpg');)
      {
        test: /\.(jpg|png|gif)$/, //check for any file that ends with jpg, png or gif and apply this loader
        include: '/assets/img/', //look only on this folder
        loader: 'url-loader' //the loader
      },
      //
      {
        test: /\.(woff|woff2|ttf|eot)?(\?*$|$)/,
        loader: 'file?name=/fonts/[name].[ext]?'
      },
    ]
  },
  plugins:[
    new webpack.LoaderOptionsPlugin({
      options:{
        //which version of JS should be considered for linting
        jshint: {
          esversion: 5,
          strict:"global",
          devel:true
        }
      }
    }),
    new UglifyJsPlugin({
      uglifyOptions: {mangle:false}
    }),
    new webpack.optimize.CommonsChunkPlugin({name:'vendor', filename:'vendor.bundle.js'})//,
    //new webpack.optimize.CommonsChunkPlugin({name:'customcss', filename:'customcss.bundle.css'}),
    /*new webpack.ProvidePlugin({})*/
    /*new HtmlWebpackPlugin({
      title: 'tmMaison',
      template: 'template.html',
      filename:'index.html'
    }),*/
  ]
};
