const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');
const concatCSS = require('gulp-concat-css');
const processhtml = require('gulp-processhtml');


//each pipe represents an action that will occur within the task
const buildDist = () => {
  //run the webpack anmd create the bundle js files for our code and vendor code
  gulp.src('./app/app.bootstrap.js')
    .pipe(webpackStream(webpackConfig),webpack)
    .pipe(gulp.dest('./dist'));
}

gulp.task('build',buildDist);
