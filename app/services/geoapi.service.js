"use strict"

var geoApiService= function ($http) {
	this.readGeoJSON = function(){
		return $http.get('data.geojson').then(
			function(success){
				return success.data;
			},
			function(error){
				console.log(error)
				return error
			})
	};
} 
			
exports.default = geoApiService;