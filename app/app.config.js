"use strict";
var moduleName = 'appConfig';
var routeProviderConfig = require('./config/route.config.js');
var locationProviderConfig = require('./config/location.config.js');

angular.module(moduleName, ['ui.router'])
    .config(routeProviderConfig.default)
    .config(locationProviderConfig.default);
exports.default = moduleName;
