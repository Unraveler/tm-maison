"use strict";
//var aboutStyle = require('./about.style.css');

var aboutComponent = {
	bindings:{},
	template:require('./about.template.html'),
	controller:function(){},
	controllerAs:'$aboutCtrl',
	transclude:true
}

exports.default = aboutComponent;
