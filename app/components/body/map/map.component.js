"use strict";
//var mapStyle = require('./map.style.css');

var leafletMapConstructor = require('../../../../assets/js/leafletMapConstructor.js');
leafletMapConstructor = leafletMapConstructor.default;
var algorithms = require('../../../../assets/js/algorithms.js');
algorithms = algorithms.default;
var d3Constructor = require('../../../../assets/js/d3Constructor.js');
d3Constructor = d3Constructor.default;
var mapComponent = {
    bindings: {
      invalidateSize:'<'
      //sidebarValues:'<'
    },
    template: require('./map.template.html'),
    controller: function(geoApiService) {
        for (var i = 0; i < length; i++) {
            body.append('div').attr('style', function(d) {
                console.log(color(i))
                return 'background-color: ' + color(i);
            });
        }
        this.sidebarValues = {
            padaria: 0,
            gare: 0,
            super: 0,
            preco: 0
        };

        this.showSidebar = false;
        this.showInfoSheet = false;
        this.toggleSidebar = function() {
            this.showSidebar = !this.showSidebar
        }
        var map = leafletMapConstructor.leafletMap('theMap', L.CRS.EPSG3857);
        map.theMap.whenReady(function() {
            leafletMapConstructor.leafletReady(map);
            leafletMapConstructor.leafletMapEvents(map.theMap)
        });
        this.onReset = function() {
            this.sidebarValues = {
                padaria: 0,
                gare: 0,
                super: 0,
                preco: 0
            };
            this.showInfoSheet = false;
            map.theMapGeoFeatures.neighbourHoods.clearLayers();
            d3Constructor.removeElement();

        };
        this.onSubmit = function(vPadaria, vGare, vSuper, vPreco) {
            this.showInfoSheet = true;
            d3Constructor.removeElement();
            map.theMapGeoFeatures.neighbourHoods.clearLayers();
            var arrayTodos = [];
            geoApiService.readGeoJSON().then(function(success) {
                angular.forEach(success.features, function(feature) {
                    var category = feature.properties.cat;
                    var res = algorithms.calcWeight(vPadaria, vGare, vSuper, vPreco, category);
                    for (var i = 0; i < res.length; i++) {
                        arrayTodos.push(res[i]);
                    }
                });
                var scale = algorithms.calcColorScale(arrayTodos);
                angular.forEach(success.features, function(feature) {
                    var category = feature.properties.cat;
                    var style = algorithms.getColor(scale, category)
                    L.geoJSON(feature, {
                        style: style
                    }).addTo(map.theMapGeoFeatures.neighbourHoods);
                });
                d3Constructor.colorScale(scale.slice(0,-1));
            }, function(error) {
                console.log(error)
            });
        };
    },
    controllerAs: '$mapCtrl',
    transclude: true
};
exports.default = mapComponent;
