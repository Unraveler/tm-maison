"use strict";
//var loginStyle = require('./login.style.css');

var loginComponent = {
	bindings:{},
	template:require('./login.template.html'),
	controller:function(){
		this.socialLogin = false;

		this.showSocialLogin = function(){
			this.socialLogin = !this.socialLogin;
			return this.socialLogin
		}
	},
	controllerAs:'$loginCtrl',
	transclude:true
}

exports.default = loginComponent;
