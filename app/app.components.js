"use strict";
var moduleName = 'appComponents';

var headerComponent = require('./components/header/header.component.js');
var bodyComponent = require('./components/body/body.component.js');
var footerComponent = require('./components/footer/footer.component.js');
var mapComponent = require('./components/body/map/map.component.js');
var aboutComponent = require('./components/body/about/about.component.js');
var contactsComponent = require('./components/body/contacts/contacts.component.js');
var homeComponent = require('./components/body/home/home.component.js');
var loginComponent = require('./components/body/login/login.component.js');

angular.module(moduleName, [])
	/*.component('about', {
  		template:  '<h3>Its the UI-Router<br>Hello Solar System app!</h3>'
	})
	.component('hello', {
  		template:  '<h3>{{$ctrl.greeting}} solar sytem!</h3>' +
             	   	'<button ng-click="$ctrl.toggleGreeting()">toggle greeting</button>',

  		controller: function() {
    		this.greeting = 'hello';
    		this.toggleGreeting = function() {
      			this.greeting = (this.greeting == 'hello') ? 'whats up' : 'hello'
    		}
  		}
	})
	.component('people', {
  		bindings: { people: '<' },
  		template: '<div class="flex-h">' +
            '  <div class="people">' +
            '    <h3>Some people:</h3>' +
            '    <ul>' +
            '      <li ng-repeat="person in $ctrl.people">' +
            '        <a ui-sref-active="active" ui-sref="people.person({ personId: person.id })">' +
            '          {{person.name}}' +
            '        </a>' +
            '      </li>' +
            '    </ul>' +
            '  </div>' +
            '  <ui-view></ui-view>' +
            '</div>'
	})
	.component('person', {
  		bindings: { person: '<' },
  		template: '<h3>A person!</h3>' +
            '<div>Name: {{$ctrl.person.name}}</div>' +
            '<div>Id: {{$ctrl.person.id}}</div>' +
            '<div>Company: {{$ctrl.person.company}}</div>' +
            '<div>Email: {{$ctrl.person.email}}</div>' +
            '<div>Address: {{$ctrl.person.address}}</div>' +

            '<button ui-sref="people">Close</button>'
	})*/
	.component('headerComponent', headerComponent.default)
	.component('bodyComponent', bodyComponent.default)
	.component('footerComponent', footerComponent.default)
	.component('mapComponent', mapComponent.default)
  .component('aboutComponent', aboutComponent.default)
  .component('contactsComponent', contactsComponent.default)
  .component('homeComponent', homeComponent.default)
  .component('loginComponent', loginComponent.default)

exports.default = moduleName;
