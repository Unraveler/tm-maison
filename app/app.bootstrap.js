"use strict";
//This file is responsible for loading the main AngularJS module and manually bootstraping the Angular app.
//We cannot use ng-app to bootstrap the application as the modules are loaded asynchronously.
var moduleName = require('./app.module.js');

//Bootstrap angular
angular.element(document).ready(function() {
	//check if the app is running through Cordova/Phonegap
	if (window.cordova) {
        console.log("Running in Cordova, will bootstrap AngularJS once 'deviceready' event fires.");
        document.addEventListener('deviceready', function() {
        	//Some cordova/phonegap diagnostics should be written here, for isntance, to check if device location is enabled.
        	console.log("Deviceready event has fired. Running into mobile device. Bootstrapping AngularJS.");
            angular.bootstrap(document.body, [moduleName.default]);
        }, false);
    } else { //if not, just continue in the browser
    	console.log("Running in browser. Bootstrapping AngularJS now.");
    	angular.bootstrap(document.body, [moduleName.default]);
    }
});
