"use strict";
var moduleName = 'appServices';
var geoApiService = require('./services/geoapi.service.js');
angular.module(moduleName, [])
	.service('geoApiService', geoApiService.default);
exports.default = moduleName;
