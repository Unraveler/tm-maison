"use strict";
//import your controls, services and directives that will be used throughout the app
var appComponentsModule = require('./app.components.js');
var appControllersModule = require('./app.controllers.js');
var appServicesModule = require('./app.services.js');
var appConfigModule = require('./app.config.js');

//name you want to give to the main module
var moduleName = 'tmMaison';
//File defining the main module. Add the external modules to use in the array.
angular.module(moduleName, ['ngAnimate', appComponentsModule.default, appControllersModule.default, appServicesModule.default, appConfigModule.default]);

exports.default = moduleName;
