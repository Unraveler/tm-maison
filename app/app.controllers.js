"use strict";
var moduleName = 'appControllers';

var rootController = require('./controllers/root.controller.js');

angular.module(moduleName, [])
	.controller('rootController', rootController.default);

exports.default = moduleName;
