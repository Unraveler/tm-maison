"use strict"

var locationProvider = function($locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
}

exports.default = locationProvider;
