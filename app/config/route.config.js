"use strict"
var routeConfig = function($stateProvider) {
    var states = [{
            name: 'home',
            url: '/',
            component:'homeComponent'
        }, {
            name: 'contacts',
            url: '/contacts',
            component: 'contactsComponent'
        }, {
            name: 'about',
            url: '/about',
            component: 'aboutComponent'
        }, {
            name: 'map',
            url: '/map',
            component: 'mapComponent'
        },{
            name:'login',
            url:'/login',
            component: 'loginComponent'
        }
        /*,
                {
                    name: 'people',
                    url: '/people',
                    component: 'people',
                    // This state defines a 'people' resolve
                    // It delegates to the PeopleService to HTTP fetch (async)
                    // The people component receives this via its `bindings: `
                    resolve: {
                        people: function(PeopleService) {
                            return PeopleService.getAllPeople();
                        }
                    }
                }, {
                    name: 'people.person',
                    url: '/{personId}',
                    component: 'person',
                        resolve: {
                            person: function(people, $stateParams) {
                                return people.find(function(person) {
                                    return person.id === $stateParams.personId;
                                });
                            }
                    }
                }*/
    ]
    // Loop over the state definitions and register them
    states.forEach(function(state) {
        $stateProvider.state(state);
    });
};
exports.default = routeConfig;
