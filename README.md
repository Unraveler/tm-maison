Description
---
Work in progress

Before you begin
---
- You will also need to configure <b>NodeJS</b>, <b>Node Package Manager(npm)</b> and <b>Grunt</b> in your system before starting. If you didn't do it already, run the following commands (on linux distros):
    - <b>NodeJS</b>: `sudo apt-get install nodejs`
    - <b>npm</b>: `sudo apt-get install npm`
    - <b>GULP</b>: `sudo npm install -g gulp-cli`

Installation
---
- `npm install`
- `gulp build`
