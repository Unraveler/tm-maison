"use strict"
var algorithms = {
    calcWeight: function(vPadaria, vGare, vSuper, vPreco, categoria) {
        var criterio = [vPadaria, vGare, vSuper, vPreco]
        //1-Somar os pesos dos valores escolhidos na barra ao lado
        var soma = 0;
        for (var i = 0; i < criterio.length; i++) {
            soma = soma + criterio[i];
        };
        //2-A cada atributo, dividir o peso escolhido pela soma dos pesos.multiplicar pelo valor da categoria.multiplicar os valores por 12
        for (var i = 0; i < criterio.length; i++) {
            criterio[i] = ((criterio[i] / soma) * categoria) * 12;
        };
        return criterio;
    },
    calcColorScale: function(arrayTodos) {
        var max = Math.max.apply(null, arrayTodos);
        var min = Math.min.apply(null, arrayTodos);
        var aux = (max - min) / 4;
        var colorScale = [min + 1 * aux, min + 2 * aux, min + 3 * aux, min + 4 * aux, min + 5 * aux, 'transparent'];
        return colorScale;
    },
    getColor: function(colorScale, category) {
        switch (category) {
            case 20:
                return {
                    fillColor: 'hsl(' + colorScale[4] + ', 100%, 50%)',
                    weight: 1,
                    opacity: 0.3,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                };
                break;
            case 16:
                return {
                    fillColor: 'hsl(' + colorScale[3] + ', 100%, 50%)',
                    weight: 1,
                    opacity: 0.3,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                }
                break;
            case 12:
                return {
                    fillColor: 'hsl(' + colorScale[2] + ', 100%, 50%)',
                    weight: 1,
                    opacity: 0.3,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                }
                break;
            case 8:
                return {
                    fillColor: 'hsl(' + colorScale[1] + ', 100%, 50%)',
                    weight: 1,
                    opacity: 0.3,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                }
                break;
            case 4:
                return {
                    fillColor: 'hsl(' + colorScale[0] + ', 100%, 50%)',
                    weight: 1,
                    opacity: 0.3,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                };
                break;
            default:
                return {
                    fillColor: colorScale[5],
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.0
                }
        }
    }
}
exports.default = algorithms;
