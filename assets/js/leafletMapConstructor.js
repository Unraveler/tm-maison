"use strict"
var L = require('leaflet');
var appNamespaces = require('./namespaces.js');
var appNamespaces = appNamespaces.default;
var leafletMapConstructor = {
    leafletMap: function(mapName, CRS) {
        var mapOptions = {
            crs: CRS,
            center: [48.86, 2.35],
            zoom: 12,
            minZoom: 5,
            maxZoom: 15,
            maxBounds: null,
            attributionControl: true,
            zoomControl: false
        };
        var mapBaselayers = {
            OpenStreetMaps: L.tileLayer(appNamespaces.baseLayers.layerOSM.url, {attribution: appNamespaces.baseLayers.layerOSM.attribution}),
            StamenToner: L.tileLayer(appNamespaces.baseLayers.layerStamen.url, {attribution: appNamespaces.baseLayers.layerStamen.attribution})
        };
        var mapOverlays = {
            Hillshade: L.tileLayer(appNamespaces.overlayLayers.Hillshade.url, {
                format: 'image/png',
                opacity: 0.45,
                transparent: true,
                attribution: appNamespaces.overlayLayers.Hillshade.attribution
            })
        };
        var mapControls = {
            layers: L.control.layers(null, null, {
                collapsed: false
            }),
            zoom: L.control.zoom({
                zoomInTitle: 'Zoom In',
                zoomOutTitle: 'Zoom Out'
            }),
            scale: L.control.scale({
                metric: true,
                imperial: false
            })/*,
            legend: L.control({
            	position: 'bottomright'
            })*/
        };
        var mapGeoFeatures = {
            neighbourHoods: L.featureGroup()
        };
        return {
            theMap: L.map(mapName, mapOptions),
            theMapBaselayers: mapBaselayers,
            theMapOverlays: mapOverlays,
            theMapControls: mapControls,
            theMapGeoFeatures: mapGeoFeatures
        };
    },
    leafletReady: function(leafletMap) {
        leafletMap.theMapBaselayers.OpenStreetMaps.addTo(leafletMap.theMap);
        //load the geofeatures
        for (var geofeature in leafletMap.theMapGeoFeatures) {
            leafletMap.theMapGeoFeatures[geofeature].addTo(leafletMap.theMap);
        };
        //load the map controllers
        for (var controller in leafletMap.theMapControls) {
            leafletMap.theMapControls[controller].addTo(leafletMap.theMap);
        };
        //add the other maps to the map
        for (var baseMap in leafletMap.theMapBaselayers) {
            leafletMap.theMapControls.layers.addBaseLayer(leafletMap.theMapBaselayers[baseMap], baseMap);
        };
        for (var overlay in leafletMap.theMapOverlays) {
            leafletMap.theMapControls.layers.addOverlay(leafletMap.theMapOverlays[overlay], overlay);
        };
    },
    leafletMapEvents: function(leafletMap) {
        leafletMap.on('movestart', function(event) {console.log('move start')});
        leafletMap.on('move', function(event) {console.log('moving')});
        leafletMap.on('moveend', function(event) {console.log('move stoped')});

    },
    addLeafletBaselayers: function(leafletMap, baselayer, baselayerName) {
        baselayer.addTo(leafletMap.theMapBaselayers);
        baselayer.addTo(leafletMap.theMap);
        leafletMap.theMapControls.layers.addBaseLayer(baselayer, baselayerName);
    },
    addLeafletOverlays: function(leafletMap, overlay, overlayName) {
        overlay.addTo(leafletMap.theMapOverlays);
        overlay.addTo(leafletMap.theMap);
        leafletMap.theMapControls.layers.addBaseLayer(overlay, overlayName);
    },
    customControls:{}
};

exports.default = leafletMapConstructor;
