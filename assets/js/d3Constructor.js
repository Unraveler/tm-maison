"use strict"
var d3Constructor = {
    colorScale: function(scaleArray) {
    	console.log(scaleArray)
    	var max = Math.max.apply(null, scaleArray);
    	var min = Math.min.apply(null, scaleArray);
    	console.log(min,max)
        var body = d3.select("#fixedContainer"),
            length = 1000,
            color = d3.scaleLinear().domain([0, 1000]).range([min, max]);
        for (var i = 0; i < length; i++) {
            body.append('div').attr('style', function(d) {
                return 'background-color: hsl(' + color(i) + ',100%,50%)';
            });
        }

    },
    removeElement: function(){
    	d3.selectAll("#fixedContainer div").data([]).exit().remove();
    }
};
exports.default = d3Constructor;
