/**
 * all URLs used in the heiMAP
 */
/**
 * appNamespaces and schemata e.g. for XML requests to services
 */
var appNamespaces = {};
appNamespaces.xls = 'http://www.opengis.net/xls';
appNamespaces.sch = 'http://www.ascc.net/xml/schematron';
appNamespaces.gml = 'http://www.opengis.net/gml';
appNamespaces.wps = 'http://www.opengis.net/wps/1.0.0';
appNamespaces.ows = 'http://www.opengis.net/ows/1.1';
appNamespaces.xlink = 'http://www.w3.org/1999/xlink';
appNamespaces.xsi = 'http://www.w3.org/2001/XMLSchema-instance';
appNamespaces.ascc = 'http://www.ascc.net/xml/schematron';
appNamespaces.aas = 'http://www.geoinform.fh-mainz.de/aas';
appNamespaces.gpx = 'http://www.topografix.com/GPX/1/1';
appNamespaces.xml = 'http://www.w3.org/XML/1998/namespace';
appNamespaces.xsd = 'http://www.w3.org/2001/XMLSchema';
appNamespaces.tcx = 'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2';
appNamespaces.gml32 = 'http://www.opengis.net/gml/3.2';
appNamespaces.xs = 'http://www.w3.org/2001/XMLSchema';
appNamespaces.kml = 'http://www.opengis.net/kml/2.2';
appNamespaces.atom = 'http://www.w3.org/2005/Atom';
appNamespaces.xal = 'urn:oasis:names:tc:ciq:xsdschema:xAL:2.0';
appNamespaces.schemata = {
    directoryService: 'http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/DirectoryService.xsd',
    analyseService: 'http://www.geoinform.fh-mainz.de/aas',
    gatewayService: 'http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/GatewayService.xsd',
    locationUtilityService: 'http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/LocationUtilityService.xsd',
    presentationService: 'http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/PresentationService.xsd',
    routeService: 'http://www.opengis.net/xls http://schemas.opengis.net/ols/1.1.0/RouteService.xsd',
    wpsService: 'http://www.opengis.net/xls http://schemas.opengis.net/wps/1.0.0/wpsExecute_request.xsd',
    lineStringService: 'http://www.opengis.net/gml http://schemas.opengis.net/gml/3.1.1/base/geometryBasic0d1d.xsd',
    gpxService: 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd',
    tcxService: 'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd',
    kmlService: 'http://www.opengis.net/kml/2.2 http://schemas.opengis.net/kml/2.2.0/ogckml22.xsd'
};
/**
 * metadata used when generating (export) files
 */
appNamespaces.metadata = {
    name: 'tmMaison',
    description: 'Exported from tmMaison app',
    authorName: 'Rui Nunes, Lino Bento',
    authorEmailId: 'some_person',
    authorEmailDomain: 'some_domain',
    copyright: '2017 @ tmMaison',
    license: 'MIT',
    link: '',
    keywords: 'Maison. Real Estate. Housing.',
    src: 'Exported from tmMaison app'
};
/**
 * map layers used on the map
 */
appNamespaces.baseLayers = {};
appNamespaces.overlayLayers = {};
//url to OSM layer
appNamespaces.baseLayers.layerOSM = {};
appNamespaces.baseLayers.layerOSM.url = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
appNamespaces.baseLayers.layerOSM.attribution = 'Map data &copy; <a href="http://www.openstreetmap.org/">OpenStreetMap</a> contributors';
//url to stamen maps
appNamespaces.baseLayers.layerStamen = {};
appNamespaces.baseLayers.layerStamen.url = 'http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png';
appNamespaces.baseLayers.layerStamen.attribution = 'Map data &copy; <a href="http://www.openstreetmap.org/">OpenStreetMap</a> contributors';
//urls to Hillshade overlay
appNamespaces.overlayLayers.Hillshade = {};
appNamespaces.overlayLayers.Hillshade.url = 'http://korona.geog.uni-heidelberg.de/tiles/asterh/x={x}&y={y}&z={z}';
appNamespaces.overlayLayers.Hillshade.attribution = '<a href="http://srtm.csi.cgiar.org/">SRTM</a>; ASTER GDEM is a product of <a href="http://www.meti.go.jp/english/press/data/20090626_03.html">METI</a> and <a href="https://lpdaac.usgs.gov/products/aster_policies">NASA</a>';


//export the appNamespaces object
exports.default = appNamespaces;